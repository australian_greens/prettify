# Email prettifier #

Ultra basic tool to provide inline HTML styles for emails. Used primarily with NationBuilder. Could be refined quite a bit.

Functions:

* Insert Greens-themed buttons
* Make images full-width
* Make links bold, Green, no underline

See it live at https://prettify.vic.greens.org.au